﻿using Photon.Pun;
using Photon.Realtime;
using RenderHeads.Media.AVProVideo;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Host
{
    /// <summary>
    /// Class used to represent a participant as a GameObject and store informations
    /// It is used to keep track of the participant video stream
    /// </summary>
    public class ParticipantGO : MonoBehaviour
    {
        /// <summary>
        /// Participant associated to the video stream
        /// </summary>
        public Participant participant;

        /// <summary>
        /// Remote video stream ID
        /// </summary>
        public string streamID;

        /// <summary>
        /// URL of the video stream
        /// </summary>
        public string streamURL;

        /// <summary>
        /// Name of the video stream as it appear on the GUI
        /// </summary>
        public string videoName;

        /// <summary>
        /// Play the video stream from the video stream URL
        /// </summary>
        public void StartPlaying()
        {
            MediaPlayer me = GetComponent<MediaPlayer>();
            Debug.Log("STREAMURL : " + streamURL);
            Debug.Log("AbsolutePathOrURL : " + MediaPlayer.FileLocation.AbsolutePathOrURL);
            me.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, streamURL);
        }

        public void LoadImage(Texture2D receivedTexture)
        {
            GetComponent<RawImage>().texture = receivedTexture;
            Debug.Log("texture set");
        }
    }
}


