﻿using System.Collections;
using System.Collections.Generic;
using Michsky.UI.ModernUIPack;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;

namespace Host.Toolbox
{


    public class WebRequestCert : CertificateHandler
    {
        protected override bool ValidateCertificate(byte[] certificateData)
        {
            //return base.ValidateCertificate(certificateData);
            return true;
        }
    }

    /// <summary>
    /// Class holding all the functions commun to classes
    /// </summary>
    public class Tools : MonoBehaviour
    {
        private Tools _instance;

        // Start is called before the first frame update
        void Start()
        {
            if (_instance != null)
            {
                Destroy(this.gameObject);
                return;
            }

            _instance = this;


            // Make sure that the the current gameObject won't be destroyed
            GameObject.DontDestroyOnLoad(this.gameObject);
        }

        /// <summary>
        /// Display a notification
        /// </summary>
        /// <param name="n">NotificationManager component</param>
        /// <param name="title">Notification title</param>
        /// <param name="content">Notificiation content</param>
        public void ShowNotification(NotificationManager n, string title, string content)
        {
            n.title = title;
            n.description = content;
            n.UpdateUI();
            n.OpenNotification();
        }

        /// <summary>
        /// Add a button on a given container
        /// </summary>
        /// <param name="btnPrefab">Prefab of the button to instanciate</param>
        /// <param name="btnName">Name of the button</param>
        /// <param name="container">Container in which the button should be placed</param>
        /// <returns>The newly created GameObject</returns>
        public GameObject AddButtonToContainer(GameObject btnPrefab, string btnName, GameObject container)
        {
            GameObject btn = Instantiate(btnPrefab) as GameObject;
            btn.GetComponentInChildren<TextMeshProUGUI>().SetText(btnName);
            btn.transform.SetParent(container.transform);
            return btn;
        }

        /// <summary>
        /// Instantiate a prefab in a container
        /// </summary>
        /// <param name="prefab">Prefab GameObject to instantiate</param>
        /// <param name="container">Container where the GameObject needs to go</param>
        /// <returns>The GameObject instantiated</returns>
        public GameObject AddPrefabToContainer(GameObject prefab, GameObject container)
        {
            GameObject go = Instantiate(prefab) as GameObject;
            go.transform.SetParent(container.transform);
            return go;
        }

        /// <summary>
        /// Remove a button with a specific name from the GUI
        /// </summary>
        /// <param name="btnName">Name of the button</param>
        /// <param name="container">Container where the button is</param>
        /// <returns>true if the button has been removed, else false</returns>
        public bool RemoveButtonByName(string btnName, GameObject container)
        {
            for (int i = 0; i < container.transform.childCount; i++)
            {
                string btnText = container.transform.GetChild(i).GetComponentInChildren<TextMeshProUGUI>().text;
                if (btnText == btnName)
                {
                    Destroy(container.transform.GetChild(i).gameObject);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Add a GameObject with text component to a container
        /// </summary>
        /// <param name="prefab">Prefab of the element to instanciate</param>
        /// <param name="text">Text of the element</param>
        /// <param name="container">Container in which the element should be placed</param>
        /// <returns></returns>
        public GameObject AddElementWithTextToContainer(GameObject prefab, string text, GameObject container)
        {
            GameObject go = Instantiate(prefab) as GameObject;
            go.GetComponentInChildren<Text>().text = text;
            go.transform.SetParent(container.transform);
            return go;
        }

        /// <summary>
        /// Add a GameObject with input text field to a container
        /// </summary>
        /// <param name="prefab">Prefab of the element to instanciate</param>
        /// <param name="text">Text of the element</param>
        /// <param name="container">Container where the element should be placed</param>
        /// <returns></returns>
        public GameObject AddElementWithInputTextToContainer(GameObject prefab, string text, GameObject container)
        {
            GameObject go = Instantiate(prefab) as GameObject;
            go.GetComponentInChildren<TMP_InputField>().text = text;
            go.transform.SetParent(container.transform);
            return go;
        }


        /// <summary>
        /// Hide a button
        /// </summary>
        /// <param name="btn">Button GameObject to hide</param>
        public void HideButton(GameObject btn)
        {
            btn.SetActive(false);
        }

        /// <summary>
        /// Show a button
        /// </summary>
        /// <param name="btn">Button GameObject to show</param>
        public void ShowButton(GameObject btn)
        {
            btn.SetActive(true);
        }

        /**
         * 
         * @param container : The parent GameObject
         **/

        /// <summary>
        /// Remove all button from a container
        /// </summary>
        /// <param name="container">Container where the buttons are</param>
        public void RemoveAllButtonFromContainer(GameObject container)
        {
            int numberOfBtn = container.transform.childCount;

            for (int i = 0; i < numberOfBtn; i++)
            {
                Destroy(container.transform.GetChild(i).gameObject);
            }
        }

        /// <summary>
        /// Empty a text input
        /// </summary>
        /// <param name="input">Input concerned</param>
        public void EmptyInput(TMP_InputField input)
        {
            input.text = string.Empty;
        }

        /// <summary>
        /// Set all the button of a container to a certain color
        /// </summary>
        /// <param name="container">Container holding the buttons</param>
        /// <param name="c">Color</param>
        public void SetColorAllBtnContainer(GameObject container, Color32 c)
        {
            for (int i = 0; i < container.transform.childCount; i++)
            {
                GameObject btn = container.transform.GetChild(i).gameObject;
                ChangeNormalColorBtn(btn, c);
            }
        }

        /// <summary>
        /// Set the normal color of a button
        /// </summary>
        /// <param name="btn">Button</param>
        /// <param name="c">Color</param>
        public void ChangeNormalColorBtn(GameObject btn, Color32 c)
        {
            Color colors = btn.GetComponent<Image>().color;
            colors = c;
            btn.GetComponent<Image>().color = colors;
        }

        /// <summary>
        /// Change the color of a button when selected
        /// </summary>
        /// <param name="btn">Button</param>
        /// <param name="container">Container in which the button is</param>
        /// <param name="regular">Regular color of the button</param>
        /// <param name="selected">Color of the selected button</param>
        public void ChangeButtonColorOnSelect(GameObject btn, GameObject container, Color32 regular, Color32 selected)
        {
            SetColorAllBtnContainer(container, regular);
            ChangeNormalColorBtn(btn, selected);
        }

        /// <summary>
        /// Perfom a HTTP GET request
        /// </summary>
        /// <param name="url">URL of the route</param>
        /// <param name="callback">Callback to execute when the request is performed</param>
        /// <returns></returns>
        public IEnumerator GetRequest(string url, Action<UnityWebRequest> callback)
        {

            using (UnityWebRequest request = UnityWebRequest.Get(url))
            {
                request.certificateHandler = new WebRequestCert();
                // Send the request and wait for a response
                yield return request.SendWebRequest();
                callback(request);
            }
        }

        /// <summary>
        /// Get an image
        /// </summary>
        /// <param name="url">URL of the image resource</param>
        /// <param name="callback">Callback to execute when the request is performed</param>
        /// <returns></returns>
        public IEnumerator GetImage(string url, Action<UnityWebRequest> callback)
        {
            using (UnityWebRequest request = UnityWebRequestTexture.GetTexture(url))
            {
                // Send the request and wait for a response
                yield return request.SendWebRequest();
                callback(request);
            }
        }

        /// <summary>
        /// Download an image from a URL and apply it on a RawImage component
        /// </summary>
        /// <param name="url">Image URL</param>
        /// <param name="go">GameObject with the Raw Image component</param>
        public void GetImageOnTexture(string url, GameObject go)
        {
            StartCoroutine(GetImage(url, (UnityWebRequest req) =>
            {
                if (req.isNetworkError || req.isHttpError)
                {
                    Debug.Log($"{req.error}: {req.downloadHandler.text}");
                }
                else
                {
                    var texture = DownloadHandlerTexture.GetContent(req);
                    if (texture != null)
                    {
                        go.GetComponent<RawImage>().texture = texture;
                    }
                    else
                    {
                        Debug.LogError("[UIReviewSimulationScene] - Couldn't get thumbnail");
                    }
                }
            }));
        }

        /// <summary>
        /// Perfom a HTTP POST request
        /// </summary>
        /// <param name="url">URL of the route</param>
        /// <param name="data">Data to send to the server</param>
        /// <param name="callback">Callback to execute when the request is performed</param>
        /// <returns></returns>
        public IEnumerator PostRequest(string url, string data, Action<UnityWebRequest> callback)
        {
            using (UnityWebRequest request = UnityWebRequest.Post(url, data))
            {
                // Send the request and wait for a response
                yield return request.SendWebRequest();
                callback(request);
            }
        }

        /// <summary>
        /// Perform a HTTP DELETE request
        /// </summary>
        /// <param name="url">URL of the route</param>
        /// <param name="callback">Callback to execute when the request is performed</param>
        /// <returns></returns>
        public IEnumerator DeleteRequest(string url, Action<UnityWebRequest> callback)
        {
            using (UnityWebRequest request = UnityWebRequest.Delete(url))
            {
                // Send the request and wait for a response
                yield return request.SendWebRequest();
                callback(request);
            }
        }
    }
}


