﻿using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Michsky.UI.ModernUIPack;
using Host.DB;
using Host.AppSettings;
using Host.Toolbox;

namespace Host.UI
{
    using Host.Network;

    /// <summary>
    /// Class holding the logic for the ReviewSimulation scene
    /// </summary>
    public class UIReviewSimulationScene : MonoBehaviour
    {
        [Header("Image comments")]
        public GameObject imagePrefab;
        public GameObject imageContainer;

        [Header("Participants")]
        public GameObject btnParticipantPrefab;
        public GameObject btnParticipantContainer;

        [Header("Stats")]
        public Text textDuration;
        public Text textTotalComment;

        [Header("Simulation")]
        public Button saveSimulationBtn;

        [Header("Notification")]
        public NotificationManager notification;


        private SimulationManager simulationManager;
        private SceneLoader sceneLoader;
        private NetworkManager networkManager;
        private Tools tools;
        private DBManager dBManager;
        private Settings currentSettings;

        // Start is called before the first frame update
        void Start()
        {
            try
            {
                simulationManager = GameObject.Find("SimulationManager").GetComponent<SimulationManager>();
                sceneLoader = GameObject.Find("SceneLoader").GetComponent<SceneLoader>();
                networkManager = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();
                tools = GameObject.Find("Tools").GetComponent<Tools>();
                dBManager = GameObject.Find("DBManager").GetComponent<DBManager>();
            }
            catch
            {
                Debug.LogError("[UIPreviousSimulationManager] - Couldn't initialize managers");
                return;
            }

            currentSettings = dBManager.GetSettings();

            SceneLoader.previousScene = "ReviewSimulationScene";

            // Add all the comment on the GUI
            simulationManager.simulationReviewed.listComments.ForEach(c =>
            {
                GameObject comment = tools.AddElementWithInputTextToContainer(imagePrefab, c.GetContent(), imageContainer);
                CommentGO com = comment.GetComponent<CommentGO>();
                com.id = c.remoteID;
                com.content = c.GetContent();

                tools.GetImageOnTexture(c.thumbnail, comment);

                // Remove a comment on click
                comment.GetComponentInChildren<Button>().onClick.AddListener(() =>
                {
                    RemoveComment(c);
                });

            });

            // Add all participants on the GUI
            simulationManager.simulationReviewed.GetParticipants().ForEach(p =>
            {
                tools.AddButtonToContainer(btnParticipantPrefab, p.GetName(), btnParticipantContainer);
            });

            // Add stats about the simulation on the GUI
            UpdateStats();

            OnSaveSimulationHandler();
        }

        /// <summary>
        /// Update the stats fields with informations about the simulation reviewed
        /// </summary>
        private void UpdateStats()
        {
            textDuration.text = "Duration : " + simulationManager.simulationReviewed.duration.ToString(@"hh\:mm\:ss");
            textTotalComment.text = "Total comments : " + simulationManager.simulationReviewed.GetNumberOfComment().ToString();
        }

        /// <summary>
        /// Handler to save a simulation
        /// </summary>
        private void OnSaveSimulationHandler()
        {
            saveSimulationBtn.onClick.AddListener(() =>
            {
                SaveAllChangedComments();
                simulationManager.SaveCurrentSimulation();
                sceneLoader.LoadScene("PickPreviousSimulationScene");
            });
        }

        /// <summary>
        /// Get the content of each comment, compare it to what it was and change it if it changed
        /// </summary>
        private void SaveAllChangedComments()
        {

            for (int i = 0; i < imageContainer.transform.childCount; i++)
            {
                CommentGO com = imageContainer.transform.GetChild(i).GetComponentInChildren<CommentGO>();
                string commentID = com.id;
                string content = com.content;

                string inputCommentText = imageContainer.transform.GetChild(i).GetComponentInChildren<TMP_InputField>().text;

                if (content != inputCommentText)
                {
                    Comment comment = simulationManager.currentSimulation.FindComment(c => c.remoteID == commentID);
                    comment.SetContent(inputCommentText);
                    UpdateCommentContentOnAPI(comment);
                }
            }
        }

        /// <summary>
        /// Delete a comment
        /// </summary>
        /// <param name="c">Comment to remove</param>
        private void RemoveComment(Comment c)
        {
            RemoveCommentFromGUI(c);
            simulationManager.currentSimulation.RemoveComment(c);
            DeleteCommentOnAPI(c);
        }

        /// <summary>
        /// Remove a comment from the GUI
        /// </summary>
        /// <param name="c">Comment to remove</param>
        private void RemoveCommentFromGUI(Comment c)
        {

            for (int i = 0; i < imageContainer.transform.childCount; i++)
            {
                GameObject comment = imageContainer.transform.GetChild(i).gameObject;
                if (comment.GetComponent<CommentGO>().id == c.remoteID)
                {
                    Destroy(comment);
                }
            }
        }

        /// <summary>
        /// Update the content of a comment on the remote server
        /// </summary>
        /// <param name="c">Comment to update</param>
        private void UpdateCommentContentOnAPI(Comment c)
        {
            string url = "http://" + currentSettings.mainServerIP + ":5000/comment/" + c.remoteID;
            Debug.Log(url);
            string data = JsonUtility.ToJson(c);
            StartCoroutine(tools.PostRequest(url, data, (UnityWebRequest req) =>
            {
                if (req.isNetworkError || req.isHttpError)
                {
                    Debug.Log($"{req.error}: {req.downloadHandler.text}");
                }
                else
                {
                    string rep = req.downloadHandler.text;
                    Debug.Log(rep);
                }
            }));
        }

        /// <summary>
        /// Delete a comment on the remote server
        /// </summary>
        /// <param name="c">Comment to remove</param>
        private void DeleteCommentOnAPI(Comment c)
        {
            string url = "http://" + currentSettings.mainServerIP + ":5000/comment/" + c.remoteID;
            Debug.Log(url);
            StartCoroutine(tools.DeleteRequest(url, (UnityWebRequest req) =>
            {
                if (req.isNetworkError || req.isHttpError)
                {
                    Debug.Log($"{req.error}: {req.downloadHandler.text}");
                }
                else
                {
                    string rep = req.downloadHandler.text;
                    Debug.Log(rep);
                }
            }));
        }


    }

}
