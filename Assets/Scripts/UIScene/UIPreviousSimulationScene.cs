﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Host.AppSettings;
using Host.DB;
using Michsky.UI.ModernUIPack;
using Host.Toolbox;

namespace Host.UI
{
    using Host.Network;

    /// <summary>
    /// Class holding the logic for the PreviousSimulation scene
    /// </summary>
    public class UIPreviousSimulationScene : MonoBehaviour
    {
        [Header("Image comments")]
        public GameObject imagePrefab;
        public GameObject imageContainer;

        [Header("Participants")]
        public GameObject btnParticipantPrefab;
        public GameObject btnParticipantContainer;

        [Header("Stats")]
        public Text textDuration;
        public Text textTotalComment;

        [Header("Simulation")]
        public Button btnDeleteSimulation;
        public Button generateDebriefingBtn;
        public Button watchDebriefingBtn;

        [Header("Notification")]
        public NotificationManager notification;

        private SimulationManager simulationManager;
        private SceneLoader sceneLoader;
        private Tools tools;
        private DBManager dBManager;
        private NetworkManager networkManager;
        private Settings currentSettings;


        // Start is called before the first frame update
        void Start()
        {
            try
            {
                simulationManager = GameObject.Find("SimulationManager").GetComponent<SimulationManager>();
                sceneLoader = GameObject.Find("SceneLoader").GetComponent<SceneLoader>();
                tools = GameObject.Find("Tools").GetComponent<Tools>();
                dBManager = GameObject.Find("DBManager").GetComponent<DBManager>();
                networkManager = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();
            }
            catch
            {
                Debug.LogError("[UIPreviousSimulationScene] - Couldn't initialize managers");
                return;
            }

            SceneLoader.previousScene = "PreviousSimulationScene";

            currentSettings = dBManager.GetSettings();
            
            // Display all the comments on the GUI
            simulationManager.simulationReviewed.listComments?.ForEach(c =>
            {
                GameObject img = tools.AddElementWithTextToContainer(imagePrefab, c.GetContent(), imageContainer);
                tools.GetImageOnTexture(c.thumbnail, img);
            });

            // Display all the participants on the GUI
            simulationManager.simulationReviewed.GetParticipants()?.ForEach(p =>
            {
                tools.AddButtonToContainer(btnParticipantPrefab, p.GetName(), btnParticipantContainer);
            });

            btnDeleteSimulation.onClick.AddListener(() =>
            {
                OnDeleteSimulationHandler(simulationManager.simulationReviewed);
            });

            // Handler for the generate debriefing button
            generateDebriefingBtn.onClick.AddListener(() =>
            {
                if (simulationManager.simulationReviewed.GetNumberOfComment() > 0)
                {
                    sceneLoader.LoadScene("GenerateDebriefingScene");
                }
                else
                {
                    tools.ShowNotification(notification, "Error", "This simulation doesn't contain any comments");
                }
            });

            // Add stats about the simulation on the GUI
            UpdateStats();

            // Handler for the watch debriefing button
            watchDebriefingBtn.onClick.AddListener(() =>
            {
                if (simulationManager.simulationReviewed.GetNumberOfComment() > 0)
                {
                    sceneLoader.LoadScene("WatchDebriefingScene");
                }
                else
                {
                    tools.ShowNotification(notification, "Error", "This simulation doesn't contain any comments");
                }
            });


        }

        /// <summary>
        /// Update the stats fields with informations about the simulation reviewed
        /// </summary>
        private void UpdateStats()
        {
            textDuration.text = "Duration : " + simulationManager.simulationReviewed.duration.ToString(@"hh\:mm\:ss");
            textTotalComment.text = "Total comments : " + simulationManager.simulationReviewed.GetNumberOfComment().ToString();
        }

        /// <summary>
        /// Handler for the deletion of a simulation
        /// </summary>
        /// <param name="s">Simulation to delete</param>
        private void OnDeleteSimulationHandler(Simulation s)
        {
            dBManager.DeleteSimulationByID(s.id);

            DeleteSimulationOnAPI(s);

            sceneLoader.LoadScene("PickPreviousSimulationScene");
        }

        /// <summary>
        /// Delete a simulation on the remote server
        /// </summary>
        /// <param name="s">Simulation to delete</param>
        private void DeleteSimulationOnAPI(Simulation s)
        {
            string url = "http://" + currentSettings.mainServerIP + ":5000/simulation/" + s.remoteID;
            Debug.Log(url);
            StartCoroutine(tools.DeleteRequest(url, (UnityWebRequest req) =>
            {
                if (req.isNetworkError || req.isHttpError)
                {
                    Debug.Log($"{req.error}: {req.downloadHandler.text}");
                }
                else
                {
                    string rep = req.downloadHandler.text;
                    Debug.Log(rep);
                }
            }));
        }
    }

}
