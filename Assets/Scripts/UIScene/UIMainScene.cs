﻿using Host.AppSettings;
using Host.DB;
using Host.Toolbox;
using Michsky.UI.ModernUIPack;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace Host.UI
{
    using Host.Network;

    /// <summary>
    /// Class holding the logic for the Main scene
    /// </summary>
    public class UIMainScene : MonoBehaviour
    {
        [Header("Messages")]
        public GameObject btnMsgPrefab;
        public GameObject msgListContainer;

        public Button btnCustomMsg;
        public TMP_InputField inputCustomMessage;

        [Header("Virtual events")]
        public GameObject btnVirtualEventPrefab;
        public GameObject virtualEventListContainer;

        [Header("Help events")]
        public GameObject btnHelpEventPrefab;
        public GameObject helpEventContainer;

        [Header("Comments")]
        public Button btnAddComment;
        public TMP_InputField inputComment;

        [Header("Notifications")]
        public NotificationManager notification;

        [Header("Simulation control")]
        public GameObject stopSimulationBtn;
        public ModalWindowManager stopSimulationModal;
        public Button stopSimulationOKBtn;
        public Text textTimeElapsed;

        [Header("Videos")]
        public GameObject videoPrefab;
        public GameObject loadImagePrefab;
        public GameObject videoListContainer;
        public GameObject mainVideoPrefab;
        public GameObject mainLoadImagePrefab;
        public GameObject mainVideoContainer;

        [Header("New Web Stream")]
        public GameObject MainVideoStreamUIPrefab;

        // Network
        private NetworkManager networkManager;

        // Scenario
        private ScenarioManager scenarioManager;

        // Simulation
        private SimulationManager simulationManager;

        private SceneLoader sceneLoader;

        private DBManager dbManager;
        private Settings currentSettings;

        // Tools
        private Tools tools;

        private string currentRecipient = "All";

        // Current video stream displayed as the main one
        private string currentVideoStream;

        // Class used for JSON deserialization
        [Serializable]
        private class _Simulation
        {
            public string id;
            public string name;
            public List<_Stream> video_streams;
        }

        // Class used for JSON deserialization
        [Serializable]
        private class _Stream
        {
            public string id;
            public string device_ip;
            public string device_name;
            public string incoming_video_url;
            public string url;
        }

        // Class used for JSON deserialization
        [Serializable]
        private class _Comment
        {
            public string id;
            public string content;
            public string thumbnail;
            public string time_in_simulation;
        }

        [Header("for debug")]
        public bool debugHololensMode = true;

        // Start is called before the first frame update
        void Start()
        {
            try
            {
                networkManager = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();
                networkManager.uiMainScene = this;

                sceneLoader = GameObject.Find("SceneLoader").GetComponent<SceneLoader>();
                scenarioManager = GameObject.Find("ScenarioManager").GetComponent<ScenarioManager>();
                simulationManager = GameObject.Find("SimulationManager").GetComponent<SimulationManager>();
                tools = GameObject.Find("Tools").GetComponent<Tools>();
                dbManager = GameObject.Find("DBManager").GetComponent<DBManager>();
            }
            catch
            {
                Debug.LogError("[UIMainScene] - Cannot initialized managers");
                return;
            }

            currentSettings = dbManager.GetSettings();

            // Serialize the data of the current simulation to JSON
            string data = JsonUtility.ToJson(simulationManager.currentSimulation);
            string url = "http://" + currentSettings.mainServerIP + ":5000/simulation";

            // Send a POST request to the server to create the simulation
            if (!debugHololensMode)
            {
                StartCoroutine(tools.PostRequest(url, data, (UnityWebRequest req) =>
                {
                    if (req.isNetworkError || req.isHttpError)
                    {
                        Debug.Log("Salut");
                        Debug.Log($"{req.error}: {req.downloadHandler.text}");
                        tools.ShowNotification(notification, "Network Error", "Couldn't connect to server. Make sure it is on");
                    }
                    else
                    {
                        string rep = req.downloadHandler.text;
                        _Simulation si = JsonUtility.FromJson<_Simulation>(rep);
                        simulationManager.currentSimulation.SetRemoteID(si.id);

                        // Initiate all the video stream and play them
                        si.video_streams.ForEach(vs =>
                        {
                            // In the case we have the main camera
                            if (vs.device_ip == "0.0.0.0" && vs.device_name == "Main")
                            {
                                Debug.Log(vs.url);
                                string videoName = "Main Scene";

                                // Correction Debriefing generation

                                currentVideoStream = vs.id;

                                // OLD
                                GameObject mainVideo = tools.AddElementWithTextToContainer(mainVideoPrefab, videoName, mainVideoContainer);
                                ParticipantGO go = mainVideo.AddComponent<ParticipantGO>();
                                go.streamID = vs.id;
                                go.streamURL = vs.url;
                                go.videoName = videoName;
                                go.participant = null;

                                networkManager.participants.Add(go);

                                go.StartPlaying();

                                // END OLD
                                // NEW
                                //GameObject mainVideo = tools.AddElementWithTextToContainer(MainVideoStreamUIPrefab, videoName, mainVideoContainer);
                                //WebStream webStreamObject = mainVideo.GetComponentInChildren<WebStream>();
                                //if (webStreamObject != null)
                                //{
                                //    webStreamObject.streamURL = vs.url;
                                //    webStreamObject.haveCredentiel = true;
                                //    webStreamObject.username = "admin";
                                //    webStreamObject.password = "admin";
                                //    webStreamObject.GetVideo();
                                //    currentVideoStream = vs.id;
                                //}
                                // END NEW
                            }
                            else
                            {
                                // Find the participant associated to the video stream
                                Participant p = simulationManager.currentSimulation.FindParticipant((parti) => parti.GetDevice().GetIP() == vs.device_ip);
                                if (p != null)
                                {
                                    string videoName = p.GetName() + " | " + p.role;
                                    GameObject video = tools.AddElementWithTextToContainer(loadImagePrefab, videoName, videoListContainer);
                                    ParticipantGO go = video.AddComponent<ParticipantGO>();
                                    go.participant = p;
                                    go.streamID = vs.id;
                                    go.streamURL = vs.url;

                                    networkManager.participants.Add(go);

                                    video.GetComponent<Button>().onClick.AddListener(() => SwitchMainVideo(video));

                                }
                                else
                                {
                                    Debug.LogError("[UIMainScene] - Couldn't find participant with device ip " + vs.device_ip);
                                }
                            }

                        });
                    }

                }));
            }

            // Add predefined messages on GUI
            scenarioManager.currentScenario.messageEvents.ForEach(m =>
            {
                GameObject btn = tools.AddButtonToContainer(btnMsgPrefab, m.content, msgListContainer);
                btn.GetComponent<Button>().onClick.AddListener(() =>
                {
                    m.SetRecipient(currentRecipient);
                    if (networkManager.SendEvent(m))
                    {
                        tools.ShowNotification(notification, "Success", "Message sent !");
                    }
                });
            });

            // Add virtual event on GUI
            scenarioManager.currentScenario.virtualEvents.ForEach(e =>
            {
                GameObject btn = tools.AddButtonToContainer(btnVirtualEventPrefab, e.name, virtualEventListContainer);
                btn.GetComponent<Button>().onClick.AddListener(() =>
                {
                    if (networkManager.SendEvent(e))
                    {
                        tools.ShowNotification(notification, "Success", "Event triggered !");
                    }
                });
            });

            // Add help event on GUI
            scenarioManager.currentScenario.helpEvents.ForEach(h =>
            {
                GameObject btn = tools.AddButtonToContainer(btnHelpEventPrefab, h.name, helpEventContainer);
                btn.GetComponent<Button>().onClick.AddListener(() =>
                {
                    if (networkManager.SendEvent(h))
                    {
                        tools.ShowNotification(notification, "Success", "Help triggered !");
                    }
                });
            });

            // Set handler for custom message button
            OnSendCustomMessageHandler(currentRecipient);

            // Set handler for comment button
            OnAddCommentHandler();

            // Set handler for stop simulation button
            OnStopSimulationHandler();

            // start the video streaming
            networkManager.StartStreamingPhotoCapture();
        }

        // Update is called once per frame
        void Update()
        {
            // Update the time elapsed on the GUI
            textTimeElapsed.text = simulationManager.currentSimulation.GetTimeElapsed();
        }

        /// <summary>
        /// Callback when the app stops
        /// </summary>
        void OnApplicationQuit()
        {
            StopSimulationInAPI();
            networkManager.StopRemoteSimulation();
        }

        /// <summary>
        /// Switch the main video with an other one
        /// </summary>
        /// <param name="video">The video to switch</param>
        public void SwitchMainVideo(GameObject video)
        {
            networkManager.canSetTexture = false;

            GameObject mainVideo = mainVideoContainer.transform.GetChild(0).gameObject;

            if (mainVideo == null)
            {
                Debug.LogError("The main video isn't displayed");
                return;
            }

            ParticipantGO mainVideoGO = mainVideo.GetComponent<ParticipantGO>();

            ParticipantGO videoGO = video.GetComponent<ParticipantGO>();

            Destroy(mainVideo);

            GameObject witchMainPrefab = mainVideoPrefab;
            if (videoGO.participant != null)
            {
                witchMainPrefab = mainLoadImagePrefab;
            }

            // Assign the new main video with the video selected 
            GameObject newMainVideo = tools.AddElementWithTextToContainer(witchMainPrefab, videoGO.videoName, mainVideoContainer);
            ParticipantGO newMainVideoGO = newMainVideo.AddComponent<ParticipantGO>();
            newMainVideoGO.participant = videoGO.participant;
            newMainVideoGO.streamID = videoGO.streamID;
            newMainVideoGO.streamURL = videoGO.streamURL;
            newMainVideoGO.videoName = videoGO.videoName;

            if (newMainVideoGO.participant == null)
            {
                newMainVideoGO.StartPlaying();
            }
            else
            {
                List<ParticipantGO> tmpParticipants = new List<ParticipantGO>(networkManager.participants);
                foreach (ParticipantGO go in tmpParticipants)
                {
                    if (go.participant != null)
                    {
                        if (go.participant.GetDevice().GetHostname() == newMainVideoGO.participant.GetDevice().GetHostname())
                        {
                            networkManager.participants.Add(newMainVideoGO);
                            networkManager.participants.Remove(go);
                        }
                    }
                }
            }

            currentVideoStream = newMainVideoGO.streamID;

            if (newMainVideoGO.participant == null)
            {
                currentRecipient = "All";
            }
            else
            {
                currentRecipient = newMainVideoGO.participant.GetDevice().GetIP();
            }

            GameObject witchPrefab = videoPrefab;
            if (mainVideoGO.participant != null)
            {
                witchPrefab = loadImagePrefab;
            }

            // Take the previous main video and add it as a regular video
            GameObject newVideo = tools.AddElementWithTextToContainer(witchPrefab, mainVideoGO.videoName, videoListContainer);
            ParticipantGO newVideoGO = newVideo.AddComponent<ParticipantGO>();
            newVideoGO.participant = mainVideoGO.participant;
            newVideoGO.streamID = mainVideoGO.streamID;
            newVideoGO.streamURL = mainVideoGO.streamURL;
            newVideoGO.videoName = mainVideoGO.videoName;

            if (newVideoGO.participant == null)
            {
                newVideoGO.StartPlaying();
            }
            else
            {
                List<ParticipantGO> tmpParticipants = new List<ParticipantGO>(networkManager.participants);
                foreach (ParticipantGO go in tmpParticipants)
                {
                    if (go.participant != null)
                    {
                        if (go.participant.GetDevice().GetHostname() == newVideoGO.participant.GetDevice().GetHostname())
                        {
                            networkManager.participants.Add(newVideoGO);
                            networkManager.participants.Remove(go);
                        }
                    }
                }
            }

            newVideo.GetComponent<Button>().onClick.AddListener(() => SwitchMainVideo(newVideo));
            Destroy(video);

            networkManager.canSetTexture = true;
        }

        /// <summary>
        /// Handler for the send custom message button
        /// </summary>
        /// <param name="recipientIP">Recipient's IP address</param>
        private void OnSendCustomMessageHandler(string recipientIP)
        {
            btnCustomMsg.onClick.RemoveAllListeners();
            btnCustomMsg.onClick.AddListener(() =>
            {
                string message = inputCustomMessage.text;

                if (message.Length < 1)
                {
                    tools.ShowNotification(notification, "Error", "Couldn't send message, input empty !");
                    return;
                }

                if (networkManager.SendEvent(new MessageEvent(scenario: scenarioManager.currentScenario.id, type: "Alert", content: message, recipient: recipientIP)))
                {
                    tools.ShowNotification(notification, "Success", "Message sent !");
                    inputCustomMessage.text = "";
                }
            });
        }

        /// <summary>
        /// Handler for the add comment button
        /// </summary>
        public void OnAddCommentHandler()
        {
            btnAddComment.onClick.RemoveAllListeners();
            btnAddComment.GetComponent<Button>().onClick.AddListener(() =>
            {
                string content = inputComment.text;
                if (content.Length < 1)
                {
                    tools.ShowNotification(notification, "Error", "Couldn't add comment, input empty !");
                    return;
                }
                string timeInSimulation = simulationManager.currentSimulation.GetTimeElapsed();
                Comment c = new Comment(content, currentVideoStream, timeInSimulation);
                simulationManager.currentSimulation.AddComment(c);
                inputComment.text = "";

                Debug.Log("LOOK DATAS");
                Debug.Log($"content : {c.GetContent()}, currentVideoStream : {c.GetVideoStreamID()}, timeInSimulation : {c.GetTimeInSimulation()}");

                PostComment(c);

                tools.ShowNotification(notification, "Success", "Comment added !");
            });
        }

        /// <summary>
        /// Handler for the stop simulation button
        /// </summary>
        public void OnStopSimulationHandler()
        {
            stopSimulationBtn.GetComponent<Button>().onClick.AddListener(() =>
            {
                stopSimulationOKBtn.onClick.RemoveAllListeners();
                stopSimulationOKBtn.onClick.AddListener(() =>
                {
                    networkManager.StopRemoteSimulation();
                    simulationManager.simulationReviewed = simulationManager.currentSimulation;
                    StopSimulationInAPI();
                    sceneLoader.LoadScene("ReviewSimulationScene");
                });
                stopSimulationModal.OpenWindow();
                simulationManager.currentSimulation.StopTimer();
            });
        }

        /// <summary>
        /// Post a comment on the API
        /// </summary>
        /// <param name="c">Comment to post</param>
        public void PostComment(Comment c)
        {
            string data = JsonUtility.ToJson(c);
            Debug.Log("LOOK DATAS");
            Debug.Log(data);
            string url = "http://" + currentSettings.mainServerIP + ":5000/simulation/" + simulationManager.currentSimulation.remoteID + "/comment";
            Debug.Log(url);
            StartCoroutine(tools.PostRequest(url, data, (UnityWebRequest req) =>
            {
                if (req.isNetworkError || req.isHttpError)
                {
                    Debug.Log($"{req.error}: {req.downloadHandler.text}");
                }
                else
                {
                    string rep = req.downloadHandler.text;
                    Debug.Log(rep);
                    _Comment comment = JsonUtility.FromJson<_Comment>(rep);
                    c.SetRemoteID(comment.id);
                    c.SetThumbnail(comment.thumbnail);
                }
            }));
        }

        /// <summary>
        /// Send a request on the API to stop the simulation
        /// </summary>
        public void StopSimulationInAPI()
        {
            string url = "http://" + currentSettings.mainServerIP + ":5000/simulation/" + simulationManager.currentSimulation.remoteID + "/save";
            Debug.Log(url);
            StartCoroutine(tools.PostRequest(url, "", (UnityWebRequest req) =>
            {
                if (req.isNetworkError || req.isHttpError)
                {
                    Debug.Log($"{req.error}: {req.downloadHandler.text}");
                }
                else
                {
                    string rep = req.downloadHandler.text;
                    Debug.Log(rep);
                }

            }));
        }
    }
}

