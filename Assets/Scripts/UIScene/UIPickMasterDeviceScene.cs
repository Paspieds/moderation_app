﻿using Host.Network;
using Michsky.UI.ModernUIPack;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
using Host.Toolbox;

namespace Host.UI
{
    /// <summary>
    /// Class holding the logic for the PickMasterDevice scene
    /// </summary>
    public class UIPickMasterDeviceScene : MonoBehaviour
    {
        [Header("Devices")]
        public GameObject btnDevicePrefab;
        public GameObject btnDeviceContainer;

        [Header("Scene management")]
        public string nextScene;
        private SceneLoader sceneLoader;

        [Header("Instructions")]
        public Text instruText;
        public GameObject btnReady;

        [Header("Notifications")]
        public NotificationManager notification;

        public GameObject loadingBar;

        private Tools tools;
        private NetworkManager networkManager;
        private int deviceReadyCount = 0;

        PhotonView photonView;

        // Start is called before the first frame update
        void Start()
        {
            try
            {
                sceneLoader = GameObject.Find("SceneLoader").GetComponent<SceneLoader>();
                tools = GameObject.Find("Tools").GetComponent<Tools>();
                networkManager = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();
            }
            catch
            {
                Debug.LogError("[UIPickMasterDevice] - Cannot initialize managers");
                return;
            }

            photonView = PhotonView.Get(this);

            // Add its self to the network manager so it can be notified
            networkManager.uiPickMasterDeviceScene = this;

            btnReady.GetComponent<Button>().onClick.AddListener(() => OnReadyForPlacementHandler());
            if (networkManager.GetNumberOfOtherDevice() == 0)
            {
                btnReady.SetActive(false);
            }

            // Display the connected devices on the GUI
            networkManager.GetOtherConnectedPlayer().ForEach(p => CreateDeviceButton(p));

            Debug.Log("RoomReady");
            photonView.RPC("RoomReady", RpcTarget.Others);
        }

        #region Photon RPCs

        /// <summary>
        /// Called when an HoloLens is connected
        /// </summary>
        /// <param name="info">Info about the device</param>
        [PunRPC]
        public void HololensConnected(PhotonMessageInfo info)
        {
            Debug.Log("Player has connected with ID " + info.Sender.NickName + " IP : " + info.Sender.CustomProperties["ip"]);
            Player p = info.Sender;

            for (int i = 0; i < btnDeviceContainer.transform.childCount; i++)
            {
                string ip = btnDeviceContainer.transform.GetChild(i).GetComponentInChildren<DeviceGO>().ip;

                if (ip == p.CustomProperties["ip"].ToString())
                {
                    btnDeviceContainer.transform.GetChild(i).Find("Loop").gameObject.SetActive(false);
                    btnDeviceContainer.transform.GetChild(i).Find("Ready").gameObject.SetActive(true);
                }
            }
            photonView.RPC("DisplayHostname", p);
        }

        /// <summary>
        /// Display the hostname on the HoloLens
        /// </summary>
        [PunRPC]
        public void DisplayHostname()
        {
            Debug.Log("[UIPickMasterDeviceScene] - Display hostname requested");
        }

        /// <summary>
        /// Called by the Host when a Hololens needs to place the geometry
        /// </summary>
        [PunRPC]
        public void StartPlacement()
        {
            Debug.Log("[UIPickMasterDeviceScene] - Placement has been requested");
        }

        /// <summary>
        /// Called by the HoloLens when the scene has been placed
        /// </summary>
        /// <param name="info">Info about the HoloLens which placed the scene</param>
        [PunRPC]
        public void WorldAnchorPlaced(PhotonMessageInfo info)
        {
            Player p = info.Sender;

            for (int i = 0; i < btnDeviceContainer.transform.childCount; i++)
            {
                string ip = btnDeviceContainer.transform.GetChild(i).GetComponentInChildren<DeviceGO>().ip;

                if (ip == p.CustomProperties["ip"].ToString())
                {
                    btnDeviceContainer.transform.GetChild(i).Find("Loop").gameObject.SetActive(false);
                    btnDeviceContainer.transform.GetChild(i).Find("Ready").gameObject.SetActive(true);
                }
            }

            deviceReadyCount += 1;

            if(deviceReadyCount == networkManager.GetNumberOfOtherDevice())
            {
                sceneLoader.LoadScene(nextScene);
                photonView.RPC("StartSimulation", RpcTarget.Others);
            }
        }

        /// <summary>
        /// Triggered when the HoloLenses are ready
        /// </summary>
        [PunRPC]
        public void HololensReady()
        {
            Debug.Log("[UIPickMasterDeviceScene] - HoloLenses ready");
            instruText.text = "Everything is ready";
        }

        /// <summary>
        /// Triggered on the HoloLenses to start the simulation
        /// </summary>
        [PunRPC]
        public void StartSimulation() { }

        #endregion

        #region NetworkManager callbacks

        /// <summary>
        /// Triggered when a device enter the Photon Room
        /// </summary>
        /// <param name="p">Information about the device</param>
        public void OnPlayerEnteredRoom(Player p)
        {
            CreateDeviceButton(p);
                btnReady.SetActive(true);

            // Send notification to hololens to say that the we are ready to go
            Debug.Log("RoomReady");
            photonView.RPC("RoomReady", p);
        }

        /// <summary>
        /// Remove the device button when it leave the room
        /// </summary>
        /// <param name="p"></param>
        public void OnPlayerLeftRoom(Player p)
        {
            for (int i = 0; i < btnDeviceContainer.transform.childCount; i++)
            {
                string ip = btnDeviceContainer.transform.GetChild(i).GetComponentInChildren<DeviceGO>().ip;
                if (p.CustomProperties["ip"].ToString() == ip)
                {
                    Destroy(btnDeviceContainer.transform.GetChild(i).gameObject);
                    deviceReadyCount--;
                }
            }
            if (networkManager.GetNumberOfOtherDevice() == 0)
            {
                instruText.text = "Turn on all the HoloLens and scan the room, they will appear above once ready !";
                btnReady.SetActive(false);
            }
        }

        #endregion

        #region Buttons Handlers

        /// <summary>
        /// Handler that trigger the placement of the scene on the HoloLenses
        /// </summary>
        private void OnReadyForPlacementHandler()
        {
            btnReady.SetActive(false);
            Debug.Log("[UIPickMasterDeviceScene] - Scene placement requested to Hololens");
            photonView.RPC("StartPlacement", RpcTarget.Others);
            instruText.text = "Please place the scene on the HoloLens";

            for (int i = 0; i < btnDeviceContainer.transform.childCount; i++)
            {
                btnDeviceContainer.transform.GetChild(i).Find("Ready").gameObject.SetActive(false);
                btnDeviceContainer.transform.GetChild(i).Find("Loop").gameObject.SetActive(true);
            }
        }

        #endregion

        /// <summary>
        /// Creates a button on the GUI with the devices informations
        /// </summary>
        /// <param name="p">Player object associated to the device</param>
        private void CreateDeviceButton(Player p)
        {
            string btnName = p.NickName + " | " + p.CustomProperties["ip"].ToString();
            GameObject btn = tools.AddButtonToContainer(btnDevicePrefab, btnName, btnDeviceContainer);
            DeviceGO _d = btn.AddComponent<DeviceGO>();
            _d.hostname = p.NickName;
            _d.ip = p.CustomProperties["ip"].ToString();

            btn.transform.Find("Loop").gameObject.SetActive(true);
        }

    }

}
