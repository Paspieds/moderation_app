﻿using UnityEngine;
using UnityEngine.UI;
using Host.DB;
using Host.Toolbox;

namespace Host.UI
{
    /// <summary>
    /// Class holding the logic for the PickScenario scene
    /// </summary>
    public class UIPickScenarioScene : MonoBehaviour
    {
        [Header("Scenarios")]
        public GameObject btnScenarioPrefab;
        public GameObject btnScenarioContainer;

        [Header("Scene management")]
        public string nextScene;
        private SceneLoader sceneLoader;

        private ScenarioManager scenarioManager;

        private Tools tools;
        private DBManager dBManager;

        // Start is called before the first frame update
        void Start()
        {
            try
            {
                scenarioManager = GameObject.Find("ScenarioManager").GetComponent<ScenarioManager>();
                sceneLoader = GameObject.Find("SceneLoader").GetComponent<SceneLoader>();
                tools = GameObject.Find("Tools").GetComponent<Tools>();
                dBManager = GameObject.Find("DBManager").GetComponent<DBManager>();
            }
            catch
            {
                Debug.LogError("[ScenarioDisplayManager] - Cannot initialize managers");
                return;
            }

            // Display all the available scenarios on the GUI
            dBManager.GetAllScenario()?.ForEach(s =>
            {
                GameObject btn = tools.AddButtonToContainer(btnScenarioPrefab, s.name, btnScenarioContainer);
                btn.GetComponent<Button>().onClick.AddListener(() => {
                    scenarioManager.currentScenario = s;
                    sceneLoader.LoadScene(nextScene);
                });
            });
        }
    }

}

