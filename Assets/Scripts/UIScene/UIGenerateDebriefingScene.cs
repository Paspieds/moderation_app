﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Host.AppSettings;
using Host.DB;
using Host.Toolbox;

namespace Host.UI
{
    using Host.Network;

    /// <summary>
    /// Class holding the logic for the GenerateDebriefing scene
    /// </summary>
    public class UIGenerateDebriefingScene : MonoBehaviour
    {
        [Header("Buttons")]
        public Button btnPDF;
        public Button btnVideo;
        public GameObject btnBack;

        private SimulationManager simulationManager;
        private NetworkManager networkManager;
        private Tools tools;
        private DBManager dbManager;
        private Settings currentSettings;

        [Serializable]
        public class _PDF
        {
            public string pdf_url;
        }

        [Serializable]
        public class _Video
        {
            public string video_url;
        }


        // Start is called before the first frame update
        void Start()
        {
            try
            {
                simulationManager = GameObject.Find("SimulationManager").GetComponent<SimulationManager>();
                networkManager = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();
                dbManager = GameObject.Find("DBManager").GetComponent<DBManager>();
                tools = GameObject.Find("Tools").GetComponent<Tools>();
            }
            catch
            {
                Debug.LogError("[UIGenerateDebriefingScene] - Couldn't initialize managers");
                return;
            }

            currentSettings = dbManager.GetSettings();

            btnPDF.onClick.AddListener(() =>
            {
                string url = "http://" + currentSettings.mainServerIP + ":5000/simulation/" + simulationManager.simulationReviewed.remoteID + "/debrief/pdf";
                GeneratePDFRequest(url);
            });

            btnVideo.onClick.AddListener(() =>
            {
                string url = "http://" + currentSettings.mainServerIP + ":5000/simulation/" + simulationManager.simulationReviewed.remoteID + "/debrief/video";
                GenerateVideoRequest(url);
            });

            // Get the previous scene so it can come back to it when the back button is pressed
            btnBack.GetComponent<Button>().onClick.AddListener(() =>
            {
                SceneLoader sl = new SceneLoader();
                sl.LoadScene(SceneLoader.previousScene);
            });

        }

        /// <summary>
        /// Perform an HTTP request to generate a PDF debriefing
        /// </summary>
        /// <param name="url">URL of the route</param>
        private void GeneratePDFRequest(string url)
        {
            StartCoroutine(tools.GetRequest(url, (UnityWebRequest req) =>
            {
                if (req.isNetworkError || req.isHttpError)
                {
                    Debug.Log($"{req.error}: {req.downloadHandler.text}");
                }
                else
                {
                    string rep = req.downloadHandler.text;
                    Debug.Log(rep);
                    _PDF pdf = JsonUtility.FromJson<_PDF>(rep);
                    Application.OpenURL(pdf.pdf_url);
                }

            }));
        }

        /// <summary>
        /// Perform an HTTP request to generate a video debriefing
        /// </summary>
        /// <param name="url">URL of the route</param>
        private void GenerateVideoRequest(string url)
        {
            StartCoroutine(tools.GetRequest(url, (UnityWebRequest req) =>
            {
                if (req.isNetworkError || req.isHttpError)
                {
                    Debug.Log($"{req.error}: {req.downloadHandler.text}");
                }
                else
                {
                    string rep = req.downloadHandler.text;
                    Debug.Log(rep);
                    _Video video = JsonUtility.FromJson<_Video>(rep);
                    Application.OpenURL(video.video_url);
                }

            }));
        }
    }

}