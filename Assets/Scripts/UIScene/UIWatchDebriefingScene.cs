﻿using Host.AppSettings;
using Host.DB;
using Michsky.UI.ModernUIPack;
using RenderHeads.Media.AVProVideo;
using System;
using UnityEngine;
using UnityEngine.Networking;
using Host.Toolbox;

namespace Host.UI
{
    /// <summary>
    /// Class holding the logic for the WatchDebriefing scene
    /// </summary>
    public class UIWatchDebriefingScene : MonoBehaviour
    {
        [Header("Video")]
        public GameObject videoContainer;
        public GameObject videoPrefab;

        [Header("UI elements")]
        public GameObject loop;
        public NotificationManager notification;

        private SimulationManager simulationManager;
        private Tools tools;
        private DBManager dbManager;
        private Settings currentSettings;

        [Serializable]
        public class _Video
        {
            public string video_url;
        }

        // Start is called before the first frame update
        void Start()
        {
            try
            {
                simulationManager = GameObject.Find("SimulationManager").GetComponent<SimulationManager>();
                tools = GameObject.Find("Tools").GetComponent<Tools>();
                dbManager = GameObject.Find("DBManager").GetComponent<DBManager>();
            }
            catch
            {
                Debug.LogError("[UIWatchDebriefingScene] - Couldn't initialize managers");
                return;
            }

            currentSettings = dbManager.GetSettings();

            string url = "http://" + currentSettings.mainServerIP + ":5000/simulation/" + simulationManager.simulationReviewed.remoteID + "/debrief/video";

            // Perform an HTTP request to get the URL of the video debriefing
            StartCoroutine(tools.GetRequest(url, (UnityWebRequest req) =>
            {
                if (req.isNetworkError || req.isHttpError)
                {
                    Debug.Log($"{req.error}: {req.downloadHandler.text}");
                    tools.ShowNotification(notification, "Error", "Couldn't get video");
                }
                else
                {
                    string rep = req.downloadHandler.text;
                    Debug.Log(rep);
                    _Video video = JsonUtility.FromJson<_Video>(rep);

                    // Play the video once the URL received
                    if (PlayVideo(video.video_url))
                    {
                        loop.SetActive(false);
                    }
                    else
                    {
                        tools.ShowNotification(notification, "Error", "Couldn't play video");
                    }
                }

            }));
        }

        /// <summary>
        /// Play the video debriefing based on its URL
        /// </summary>
        /// <param name="url">URL of the video debriefing</param>
        /// <returns>True if it worked, else False</returns>
        private bool PlayVideo(string url)
        {
            GameObject video = tools.AddPrefabToContainer(videoPrefab, videoContainer);
            MediaPlayer m = video.GetComponent<MediaPlayer>();
            return m.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, url);
        }
    }

}
