﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System.Net;
using System.Net.Sockets;
using Michsky.UI.ModernUIPack;
using System;
using Host.UI;
using Host.DB;
using Host.Toolbox;

namespace Host.Network
{
    /// <summary>
    /// Establish the connection with the Photon PUN2 server and trigger remote events
    /// </summary>
    public class NetworkManager : MonoBehaviourPunCallbacks
    {
        /// <summary>
        /// Name of the Photon Room
        /// </summary>
        public string roomName;

        /// <summary>
        /// Number of player max in the Room
        /// </summary>
        public int numberOfPlayer;

        /// <summary>
        /// Instance of the NetworkManager object
        /// </summary>
        static NetworkManager _instance;

        // Subscribers to notification events
        [HideInInspector]
        public UIMainScene uiMainScene;
        [HideInInspector]
        public UIPickMasterDeviceScene uiPickMasterDeviceScene;

        /// <summary>
        /// NotificationManager component to trigger visual notifications
        /// </summary>
        private NotificationManager notification;

        /// <summary>
        /// Toolbox with commun functions
        /// </summary>
        private Tools tools;

        /// <summary>
        /// Database manager
        /// </summary>
        private DBManager dBManager;

        /// <summary>
        /// Photon View used to trigger RPCs
        /// </summary>
        PhotonView photonView;

        [HideInInspector]
        public List<ParticipantGO> participants = new List<ParticipantGO>();
        [HideInInspector]
        public bool canSetTexture = true;

        void Start()
        {
            if (_instance != null)
            {
                Destroy(this.gameObject);
                return;
            }

            _instance = this;

            // Make sure that the the current gameObject won't be destroyed
            GameObject.DontDestroyOnLoad(this.gameObject);

            // Initialization of the different managers
            try
            {
                dBManager = GameObject.Find("DBManager").GetComponent<DBManager>();
                tools = GameObject.Find("Tools").GetComponent<Tools>();
                notification = GameObject.Find("Notification").GetComponent<NotificationManager>();
            }
            catch
            {
                Debug.LogError("[NetworkManager] - Cannot initialize managers");
                return;
            }

            // Create the Photon View component to avoid duplicate issue
            if (photonView == null)
            {
                photonView = this.gameObject.AddComponent<PhotonView>();
                photonView.ViewID = 1;
            }

            // Connect to the Photon server after 1 second
            StartCoroutine(CallInitConnectionAfter(1.0f));

        }

        /// <summary>
        /// Establish a connection to the server after a delay
        /// </summary>
        /// <param name="time">Delay time</param>
        /// <returns></returns>
        public IEnumerator CallInitConnectionAfter(float time)
        {
            yield return new WaitForSeconds(time);
            InitConnection();
        }

        /// <summary>
        /// Establish a connection to the Photon PUN2 server
        /// </summary>
        public void InitConnection()
        {
            // If already connected, we disconnect so we can have the latest settings
            if (PhotonNetwork.IsConnected) { PhotonNetwork.Disconnect(); }

            try
            {
                PhotonNetwork.ConnectToMaster(dBManager.GetSettings().mainServerIP, 5055, "");
            }
            catch (NullReferenceException)
            {
                tools.ShowNotification(notification, "Error", "Impossible to connect to Photon server");
            }
        }

        #region Photon callbacks

        /// <summary>
        /// Callback when connect to the master server
        /// </summary>
        public override void OnConnectedToMaster()
        {
            tools.ShowNotification(notification, "OK", "Connected to master");
            CreateRoom(roomName, numberOfPlayer);
        }


        /// <summary>
        /// Callback when room couldn't be created
        /// </summary>
        /// <param name="returnCode">Error code returned</param>
        /// <param name="message">Error message returned</param>
        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            Debug.LogError("[NetworkManager] - Failed creating room : " + message);
            if (notification != null)
            {
                tools.ShowNotification(notification, "Error", "Impossible to connect to Photon server");
            }
        }

        /// <summary>
        /// Callback when room joined
        /// </summary>
        public override void OnJoinedRoom()
        {
            Debug.Log("[NetworkManager] - Created room " + PhotonNetwork.CurrentRoom);
        }

        /// <summary>
        /// Callback when a new device connects to the same room
        /// </summary>
        /// <param name="pl">Device that joined the room</param>
        public override void OnPlayerEnteredRoom(Player pl)
        {
            if (uiPickMasterDeviceScene != null)
            {
                uiPickMasterDeviceScene.OnPlayerEnteredRoom(pl);
            }

            Debug.Log("[NetworkManager] - " + pl.NickName + " entered the room");
        }

        /// <summary>
        /// Callback when device leaves the room
        /// </summary>
        /// <param name="pl">Device that left the room</param>
        public override void OnPlayerLeftRoom(Player pl)
        {
            Debug.Log("[NetworkManager] - " + pl.NickName + " left the room");
            if (uiPickMasterDeviceScene != null)
            {
                uiPickMasterDeviceScene.OnPlayerLeftRoom(pl);
            }
        }

        #endregion

        /// <summary>
        /// Create a Photon Room
        /// </summary>
        /// <param name="name">Room name</param>
        /// <param name="maxPlayer">Maximum number of player</param>
        public void CreateRoom(string name, int maxPlayer)
        {
            RoomOptions roomOptions = new RoomOptions();
            roomOptions.IsVisible = false;
            roomOptions.MaxPlayers = (byte)maxPlayer;
            PhotonNetwork.NickName = SystemInfo.deviceName;

            // Add the IP address to the custom properties
            ExitGames.Client.Photon.Hashtable details = new ExitGames.Client.Photon.Hashtable();
            details.Add("ip", GetLocalIPAddress());
            PhotonNetwork.SetPlayerCustomProperties(details);

            Debug.Log("[NetworkManager] - Creating room");

            PhotonNetwork.CreateRoom(name, roomOptions, null);

        }

        /// <summary>
        /// Send virtual, help and message events to devices
        /// </summary>
        /// <param name="ev">The event to send</param>
        /// <returns>True if the event was triggered, else False</returns>
        public bool SendEvent(IEvent ev)
        {
            if (ev.GetType() == typeof(MessageEvent))
            {
                MessageEvent m = (MessageEvent)ev;

                if (ev.GetRecipient() == "All")
                {
                    photonView.RPC("TriggerMessageEvent", RpcTarget.Others, m.scenario, m.type, m.content);
                    return true;
                }
                else
                {
                    photonView.RPC("TriggerMessageEvent", GetPlayerWithIP(ev.GetRecipient()), m.scenario, m.type, m.content);
                    return true;
                }
            }
            else if (ev.GetType() == typeof(VirtualEvent))
            {
                VirtualEvent v = (VirtualEvent)ev;
                photonView.RPC("TriggerVirtualEvent", RpcTarget.Others, v.scenario, v.action_number);
                return true;
            }
            else if (ev.GetType() == typeof(HelpEvent))
            {
                HelpEvent h = (HelpEvent)ev;
                photonView.RPC("TriggerHelpEvent", RpcTarget.Others, h.scenario, h.action_number);
                return true;
            }

            return false;
        }

        public void StartStreamingPhotoCapture()
        {
            Debug.Log("Start streaming photo capture");
            photonView.RPC("StartStreamingPhotoCapture", RpcTarget.Others);
        }

        /// <summary>
        /// Get the connected devices in the Room
        /// </summary>
        /// <returns>A list of Device object</returns>
        public List<Device> GetConnectedDevice()
        {
            List<Device> li = new List<Device>();

            foreach (Player p in PhotonNetwork.PlayerListOthers)
            {
                li.Add(new Device(hostname: p.NickName, ip: p.CustomProperties["ip"].ToString()));
            }
            return li;
        }

        /// <summary>
        /// Get the connected devices in the Room as a list of Player object
        /// </summary>
        /// <returns>A list of Player object</returns>
        public List<Player> GetOtherConnectedPlayer()
        {
            List<Player> l = new List<Player>();

            foreach (Player p in PhotonNetwork.PlayerListOthers)
            {
                l.Add(p);
            }

            return l;
        }

        /// <summary>
        /// Get the Player associated to a specific IP address
        /// </summary>
        /// <param name="ip">IP address of the Player</param>
        /// <returns>A Player object if found, else null</returns>
        public Player GetPlayerWithIP(string ip)
        {

            foreach (Player p in PhotonNetwork.PlayerListOthers)
            {
                if (p.CustomProperties["ip"].ToString() == ip)
                {
                    return p;
                }
            }
            return null;
        }

        /// <summary>
        /// Get the number of connected devices
        /// </summary>
        /// <returns>The number of connected device in the Room</returns>
        public int GetNumberOfOtherDevice() { return PhotonNetwork.PlayerListOthers.Length; }

        /// <summary>
        /// Get the IP address of the current device
        /// </summary>
        /// <returns>A string with the IP address of the device</returns>
        public string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "";
        }

        /// <summary>
        /// Send a RPC to stop the remote simulation
        /// </summary>
        public void StopRemoteSimulation()
        {
            photonView.RPC("StopSimulation", RpcTarget.Others, null);
        }

        /// <summary>
        /// Unity callback when the app is killed
        /// </summary>
        void OnApplicationQuit()
        {
            PhotonNetwork.LeaveRoom();
        }

        #region Photon RPCs

        [PunRPC]
        public void SetTexture(byte[] receivedByte, PhotonMessageInfo info)
        {
            if (canSetTexture)
            {
                Debug.Log("[UIMainScene] - get photo texture");

                if (receivedByte != null)
                {
                    Texture2D receivedTexture = new Texture2D(1, 1);
                    receivedTexture.LoadImage(receivedByte);

                    string deviceName = info.Sender.ToString().Split('\'')[1];

                    foreach (ParticipantGO go in participants)
                    {
                        if (go.participant != null)
                        {
                            if (deviceName == go.participant.GetDevice().GetHostname())
                            {
                                go.LoadImage(receivedTexture);
                            }
                        }
                    }
                }
                else
                {
                    Debug.LogError("receivedByte null");
                }
            }
        }

        [PunRPC]
        public void TriggerMessageEvent(int scenario, string type, string content)
        {
            Debug.Log("[NetworkManager] - Triggering message event");
        }

        [PunRPC]
        public void TriggerVirtualEvent(int scenario, string actionNumber) { }

        [PunRPC]
        public void TriggerHelpEvent(int scenario, string actionNumber) { }

        [PunRPC]
        void TransferAnchorRequest(byte[] requestData) { }

        [PunRPC]
        void StartAnchorTransfer(int dataSize) { }

        [PunRPC]
        public void StopSimulation() { }

        #endregion


    }

}
