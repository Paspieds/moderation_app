﻿using UnityEngine;

namespace Host
{
    /// <summary>
    /// Class holding the current scenario
    /// </summary>
    public class ScenarioManager : MonoBehaviour
    {
        static ScenarioManager _instance;
        public Scenario currentScenario;

        // Start is called before the first frame update
        void Start()
        {
            if (_instance != null)
            {
                Destroy(this.gameObject);
                return;
            }

            _instance = this;

            // Make sure that the the current gameObject won't be destroyed
            GameObject.DontDestroyOnLoad(this.gameObject);

        }

    }

}
