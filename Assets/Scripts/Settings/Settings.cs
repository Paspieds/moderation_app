﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Host.AppSettings
{
    /// <summary>
    /// Class representing the App settings
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// Duration of the video cuts for the debriefing
        /// </summary>
        public string cutDuration { get; private set; }

        /// <summary>
        /// URL of the main camera video stream
        /// </summary>
        public string mainVideoURL { get; private set; }

        /// <summary>
        /// Media server IP address
        /// </summary>
        public string mainServerIP { get; private set; }

        public Settings(string cutDuration, string mainVideoURL, string mainServerIP)
        {
            this.cutDuration = cutDuration;
            this.mainVideoURL = mainVideoURL;
            this.mainServerIP = mainServerIP;
        }
    }
}
