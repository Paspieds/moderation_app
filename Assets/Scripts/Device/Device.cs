﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Host
{
    /// <summary>
    /// Class representing a device
    /// </summary>
    [Serializable]
    public class Device
    {
        /// <summary>
        /// Hostname of the device
        /// </summary>
        [SerializeField] private string hostname;

        /// <summary>
        /// IP address of the device
        /// </summary>
        [SerializeField] private string ip;

        /// <summary>
        /// Username to use to login to the device
        /// </summary>
        [SerializeField] private string username;

        /// <summary>
        /// Password to use to login to the device
        /// </summary>
        [SerializeField] private string password;

        public Device(string hostname, string ip = "")
        {
            this.hostname = hostname;
            this.ip = ip;
            this.username = "host-project";
            this.password = "host-project";
        }

        /// <summary>
        /// Get the hostname of the device
        /// </summary>
        /// <returns>Hostname of the device</returns>
        public string GetHostname() { return this.hostname; }

        /// <summary>
        /// Set the hostname of the device
        /// </summary>
        /// <param name="hostname">Hostname of the device</param>
        public void SetHostname(string hostname) { this.hostname = hostname; }

        /// <summary>
        /// Get the IP address of the device
        /// </summary>
        /// <returns>IP address of the device</returns>
        public string GetIP() { return this.ip; }

        /// <summary>
        /// Set the IP address of the device
        /// </summary>
        /// <param name="ip">IP address of the device</param>
        public void SetIP(string ip) { this.ip = ip; }

        /// <summary>
        /// Get the username of the device
        /// </summary>
        /// <returns>Username of the device</returns>
        public string GetUsername() { return this.username; }

        /// <summary>
        /// Set the username of the device
        /// </summary>
        /// <param name="username">Username of the device</param>
        public void SetUsername(string username) { this.username = username; }

        /// <summary>
        /// Get the password of the device
        /// </summary>
        /// <returns>Password of the device</returns>
        public string GetPassword() { return this.password; }

        /// <summary>
        /// Set the password of the device
        /// </summary>
        /// <param name="password">Password of the device</param>
        public void SetPassword(string password) { this.password = password; }

        /// <summary>
        /// Representation of a device as a string
        /// </summary>
        /// <returns>String with the hostname and IP address of the device</returns>
        public override string ToString()
        {
            return "Hostname : " + this.hostname + " | IP : " + this.ip;
        }
    }

}
