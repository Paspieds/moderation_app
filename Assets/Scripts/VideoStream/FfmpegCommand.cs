﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System;

public class FfmpegCommand : MonoBehaviour
{

    private static List<Process> processes = new List<Process>();
    private static StreamWriter messageStream;

    public static string authenticate(string username, string password)
    {
        string auth = username + ":" + password;
        auth = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(auth));
        auth = "Basic " + auth;
        return auth;
    }

    public static void ExecuteCommandCamera(string username, string password, string deviceIp, string port,  string recordingPath)
    {
        var thread = new Thread(delegate () { RecordStreamVideoCamera(username, password, deviceIp, port, recordingPath); });
        thread.Start();
    }

    public static void ExecuteCommandHololens(string username, string password, string deviceIp, string quality, string recordingPath)
    {
        var thread = new Thread(delegate () { RecordStreamVideoHololens(username, password, deviceIp, quality, recordingPath); });
        thread.Start();
    }

    public static void ExecuteCommandStreamVlc(string username, string password, string deviceIp, string quality, string recordingPath)
    {
        var thread = new Thread(delegate () { StreamFileVlc(username, password, deviceIp, quality, recordingPath); });
        thread.Start();
    }

    private static void RecordStreamVideoHololens(string username, string password, string deviceIp, string quality, string recordingPath)
    {
        try
        {
            var processInfo = new ProcessStartInfo();
            processInfo.FileName = "ffmpeg";
            processInfo.Arguments = 
                $"-y -headers \"Authorization: {authenticate(username, password)}\" -i " +
                $"\"http://{username}:{password}@{deviceIp}/api/holographic/stream/live_{quality}.mp4?holo=true&pv=true&loopback=true\" " +
                $"-acodec copy -vcodec copy {recordingPath}";

            Process process = unityProcessParameters(processInfo);

            UnityEngine.Debug.Log("Successfully launched record stream from " + deviceIp);

            processes.Add(process);
        }
        catch (Exception e)
        {
            UnityEngine.Debug.LogError("Unable to launch app: " + e.Message);
        }
    }

    private static void RecordStreamVideoCamera(string username, string password, string deviceIp, string port, string recordingPath)
    {
        try
        {
            var processInfo = new ProcessStartInfo();
            processInfo.FileName = "ffmpeg";
            processInfo.Arguments =
                $"-y -i " +
                $"\"http://{username}:{password}@{deviceIp}:{port}/video\" " +
                $"-acodec copy -vcodec copy {recordingPath}";

            Process process = unityProcessParameters(processInfo);

            UnityEngine.Debug.Log("Successfully launched record stream from " + deviceIp);

            processes.Add(process);
        }
        catch (Exception e)
        {
            UnityEngine.Debug.LogError("Unable to launch app: " + e.Message);
        }
    }

    private static void StreamFile(string username, string password, string deviceIp, string quality, string streamPath)
    {
        try
        {
            var processInfo = new ProcessStartInfo();
            processInfo.FileName = "ffmpeg";
            /*processInfo.Arguments =
                $"-headers \"Authorization: {authenticate(username, password)}\" -i " +
                $"\"http://{username}:{password}@{deviceIp}/api/holographic/stream/live_{quality}.mp4?holo=true&pv=true&loopback=true\" " +
                $"-acodec copy -vcodec copy -movflags faststart {streamPath}";*/

            processInfo.Arguments = "-headers \"Authorization: Basic aG9zdC1wcm9qZWN0Omhvc3QtcHJvamVjdA==\" -i \"http://host-project:host-project@192.168.43.99/api/holographic/stream/live_high.mp4?holo=true&pv=true&loopback=true\" -acodec copy -vcodec copy -movflags faststart 0.0.0.0:8080/streaming.mp4";

            Process process = unityProcessParameters(processInfo);

            UnityEngine.Debug.Log("Successfully launched record stream from " + deviceIp);

            processes.Add(process);
        }
        catch (Exception e)
        {
            UnityEngine.Debug.LogError("Unable to launch app: " + e.Message);
        }
    }

    private static void StreamFileVlc(string username, string password, string deviceIp, string quality, string streamPath)
    {
        try
        {
            var processInfo = new ProcessStartInfo();
            processInfo.FileName = "vlc";
            /*processInfo.Arguments =
                $"-headers \"Authorization: {authenticate(username, password)}\" -i " +
                $"\"http://{username}:{password}@{deviceIp}/api/holographic/stream/live_{quality}.mp4?holo=true&pv=true&loopback=true\" " +
                $"-acodec copy -vcodec copy -movflags faststart {streamPath}";*/

            processInfo.Arguments = "./192.168.43.167.ts --loop :sout=#http{mux=ts,dst=:8080/} :no-sout-all :sout-keep";

            Process process = unityProcessParameters(processInfo);

            UnityEngine.Debug.Log("Successfully launched record stream from " + deviceIp);

            processes.Add(process);
        }
        catch (Exception e)
        {
            UnityEngine.Debug.LogError("Unable to launch app: " + e.Message);
        }
    }

    private static Process unityProcessParameters(ProcessStartInfo processInfo)
    {
        processInfo.UseShellExecute = false;
        processInfo.CreateNoWindow = true;
        processInfo.WorkingDirectory = System.Environment.CurrentDirectory;
        processInfo.ErrorDialog = true;
        processInfo.RedirectStandardOutput = true;
        processInfo.RedirectStandardInput = true;
        processInfo.RedirectStandardError = true;

        var process = new Process();
        process.StartInfo = processInfo;
        process.EnableRaisingEvents = true;
        process.OutputDataReceived += DataReceived;
        process.ErrorDataReceived += ErrorReceived;
        process.Start();
        process.BeginOutputReadLine();
        process.BeginErrorReadLine();
        messageStream = process.StandardInput;

        return process;
    }

    private static void DataReceived(object sender, DataReceivedEventArgs eventArgs)
    {
        UnityEngine.Debug.Log(eventArgs.Data);
    }

    private static void ErrorReceived(object sender, DataReceivedEventArgs eventArgs)
    {
        UnityEngine.Debug.Log(eventArgs.Data);
    }

    public void OnApplicationQuit()
    {
        UnityEngine.Debug.Log("OnApplicationQuit : kill commands processes");
        killProcesses();
    }
    public void OnDestroy()
    {
        UnityEngine.Debug.Log("OnDestroy : kill commands processes");
        killProcesses();
    }

    private void killProcesses()
    {
        foreach (Process p in processes)
        {
            if (p != null && !p.HasExited)
            {
                p.Kill();
            }
        }
    }
}
