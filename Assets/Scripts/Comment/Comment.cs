﻿using System;
using UnityEngine;

namespace Host
{
    /// <summary>
    /// Class representing a comment
    /// </summary>
    [Serializable]
    public class Comment
    {
        /// <summary>
        /// ID of the comment in the database
        /// </summary>
        public int id { get; private set; }

        /// <summary>
        /// ID of the comment in the media server database
        /// </summary>
        public string remoteID { get; private set; }

        /// <summary>
        /// Content of the comment
        /// </summary>
        [SerializeField] private string content;

        /// <summary>
        /// Video Stream ID in the media server database
        /// </summary>
        [SerializeField] private string videostreamID;

        /// <summary>
        /// Time where the comment was taken based on the simulation time
        /// </summary>
        [SerializeField] private string timeInSimulation;

        /// <summary>
        /// URL of the comment thumbnail in the media server
        /// </summary>
        public string thumbnail { get; private set; }

        /// <summary>
        /// ID of the simulation in the database
        /// </summary>
        public int simulation { get; private set; }

        public Comment(string content, string videostreamID, string timeInSimulation)
        {
            this.content = content;
            this.videostreamID = videostreamID;
            this.timeInSimulation = timeInSimulation;
        }

        /// <summary>
        /// Get the content of the comment
        /// </summary>
        /// <returns>The content of the comment</returns>
        public string GetContent() { return this.content; }

        /// <summary>
        /// Set the content of the comment
        /// </summary>
        /// <param name="content">Content of the comment</param>
        public void SetContent(string content) { this.content = content; }

        /// <summary>
        /// Get the thumbnail URL associated to the comment
        /// </summary>
        /// <returns>URL where we can find the thumbnail of the comment</returns>
        public string GetThumbnail() { return this.thumbnail; }

        /// <summary>
        /// Set the thumbnail URL of the comment
        /// </summary>
        /// <param name="thumbnail">Thumbnail URL</param>
        public void SetThumbnail(string thumbnail) { this.thumbnail = thumbnail; }

        /// <summary>
        /// Get the Video Stream ID of the comment
        /// </summary>
        /// <returns>The Video Stream ID of the comment in the media server database</returns>
        public string GetVideoStreamID() { return this.videostreamID; }

        /// <summary>
        /// Set the Video Stream ID from the comment
        /// </summary>
        /// <param name="videostreamID">The Video Stream ID of the comment in the media server database</param>
        public void SetVideoStreamID(string videostreamID) { this.videostreamID = videostreamID; }

        /// <summary>
        /// Get the time where the comment was taken
        /// </summary>
        /// <returns>Time where the comment was taken based on the simulation time</returns>
        public string GetTimeInSimulation() { return this.timeInSimulation; }

        /// <summary>
        /// Set the time where the comment was taken
        /// </summary>
        /// <param name="timeInVideo">Time where the comment was taken based on the simulation time</param>
        public void SetTimeInSimulation(string timeInVideo) { this.timeInSimulation = timeInVideo; }

        /// <summary>
        /// Set the database comment ID
        /// </summary>
        /// <param name="id">ID of the comment in the database</param>
        public void SetID(int id) { this.id = id; }

        /// <summary>
        /// Set the ID of the comment in the media server database
        /// </summary>
        /// <param name="id">ID of the comment in the media server database</param>
        public void SetRemoteID(string id) { this.remoteID = id; }

        /// <summary>
        /// Set the ID of the simulation in the database
        /// </summary>
        /// <param name="simulationID">Primary key of the simulation in the database</param>
        public void SetSimulation(int simulationID) { this.simulation = simulationID; }

        /// <summary>
        /// Representation of the comment as a string
        /// </summary>
        /// <returns>A string with the content of the comment</returns>
        public override string ToString()
        {
            return this.content;
        }
    }

}

