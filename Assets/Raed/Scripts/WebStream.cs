﻿using UnityEngine;
 using System.Collections;
 using System;
 using System.Net;
 using System.IO;
using UnityEngine.UI;

public class WebStream : MonoBehaviour
{

    public RawImage frameRawImage;    //Mesh for displaying video

    public string streamURL = "http://192.168.124.109:8081";

    [Header("Credential")]
    public bool haveCredentiel = true;
    [DrawIf("haveCredentiel", true)] // Show if haveCredentiel bool is true
    public string username = "admin";
    [DrawIf("haveCredentiel", true)] // Show if haveCredentiel bool is true
    public string password = "admin";

    private Texture2D texture;
    private Stream stream;

    //private void Start()
    //{
    //    GetVideo();
    //}

    public void GetVideo()
    {
        texture = new Texture2D(2, 2);
        // create HTTP request
        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(streamURL);
        //Optional (if authorization is Digest)
        if (haveCredentiel)
        {
            req.Credentials = new NetworkCredential(username, password);
        }
        // get response
        WebResponse resp = req.GetResponse();
        // get response stream
        stream = resp.GetResponseStream();
        StartCoroutine(GetFrame());
    }

    IEnumerator GetFrame()
    {
        Byte[] JpegData = new Byte[65536];

        while (true)
        {
            int bytesToRead = FindLength(stream);
            if (bytesToRead == -1)
            {
                print("End of stream");
                yield break;
            }

            int leftToRead = bytesToRead;

            while (leftToRead > 0)
            {
                leftToRead -= stream.Read(JpegData, bytesToRead - leftToRead, leftToRead);
                yield return null;
            }

            MemoryStream ms = new MemoryStream(JpegData, 0, bytesToRead, false, true);

            texture.LoadImage(ms.GetBuffer());
            frameRawImage.texture = texture;
            stream.ReadByte(); // CR after bytes
            stream.ReadByte(); // LF after bytes
        }
    }

    int FindLength(Stream stream)
    {
        int b;
        string line = "";
        int result = -1;
        bool atEOL = false;

        while ((b = stream.ReadByte()) != -1)
        {
            if (b == 10) continue; // ignore LF char
            if (b == 13)
            { // CR
                if (atEOL)
                {  // two blank lines means end of header
                    stream.ReadByte(); // eat last LF
                    return result;
                }
                if (line.StartsWith("Content-Length:"))
                {
                    result = Convert.ToInt32(line.Substring("Content-Length:".Length).Trim());
                }
                else
                {
                    line = "";
                }
                atEOL = true;
            }
            else
            {
                atEOL = false;
                line += (char)b;
            }
        }
        return -1;
    }
}